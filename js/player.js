// Not needed...
var source = document.getElementById("audioSource").src;
var title = document.getElementById("title");
var stream = source + "?nocache=" + Math.round((new Date()).getTime() / 1000);
var url = 'https://stream.radiochat.com.br:8010/7.xsl';
var intervalo;
var str = '';

let xhr = (url) => {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					    var str = xmlhttp.responseText.slice(xmlhttp.responseText.lastIndexOf(',') + 1);
						document.getElementById("title").innerHTML = str;
        }
    }
    xmlhttp.open("GET", url);
    xmlhttp.send(null);
};

// Needed...
const player = new Plyr('audio', {
	controls: ['play', 'mute', 'volume']
});

let refresh = () => {
	stream = source + "?nocache=" + Math.round((new Date()).getTime() / 1000);
	player.source = {
			type: 'audio',
			title: 'Stream',
			sources: [
					{
							src: stream,
							type: 'audio/mp3',
					}
			]
	};
	//player.load();
};

player.on('pause', event => {
	refresh();
});

document.getElementById("refresh").onclick = function(){
	refresh();
	player.play();
};

title.addEventListener("mouseover", function(){
	this.classList.remove('truncate');
});
title.addEventListener('mouseleave', function() {
	this.classList.add('truncate');
})

document.addEventListener('DOMContentLoaded', function() {
	refresh();
 	setInterval(function(){ xhr(url); }, 3000);
}, false);
// Not needed...
//window.player = player;
