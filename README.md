# Stream Player Minimal

Um player construído usando o Plyr com fortes modificações.

## Instalação

- `git clone https://gitlab.com/sistematico/stream-player-min.git /var/www/player`
- Edite a url da stream em `index.html`

## Demo

- [Stream Player Minimal](https://min.lucasbrum.net)
- [CodePen](https://codepen.io/sistematico/pen/NWWpYVO)

## Créditos

- [Plyr](https://plyr.io)
- Milhares de outros colaboradores...

## Ajude

Doe qualquer valor através do <a href="https://pag.ae/bfxkQW"><img src="https://img.shields.io/badge/pagseguro-green"></a> ou <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWHJL387XNW96&source=url"><img src="https://img.shields.io/badge/paypal-blue"></a>

## ScreenShot

![Screenshot][screenshot]

[screenshot]: https://gitlab.com/sistematico/stream-player-min/raw/master/min.png "ScreenShot"

